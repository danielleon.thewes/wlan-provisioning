#!/usr/bin/env python3
from crc import crc_8

#+++please change+++ 
filename="dump.txt" #dump file
paket_offest=44	   #paket header length to calculate data length 
#+++++++++++++++++++

#return (possible) start indices of spezific sequence
def findIndicesofSequencebyNumber(dump, sequence):
	index=[i for i,x in enumerate(dump) if x==sequence]
	return index

#return overall length of the transmission
def findLength(dump):
	indices=findIndicesofSequencebyNumber( dump, 128)
	for index in indices:
		length_upper =dump[ index-5]-16
		length_lower =dump[ index-4]-32
		length_crc_upper=dump[ index-3]-48
		length_crc_lower=dump[ index-2]-64
		length =int("{0:b}".format(length_upper)+"{0:b}".format(length_lower),2)
		length_crc =int("{0:b}".format(length_crc_upper)+"{0:b}".format(length_crc_lower),2)
		if crc_8([ length ]) == length_crc:
			return length

#return data of sequence at spezific index from dump
def extractSequence( dump, indices):
	for index in indices:		
		group = []
		group.append( dump[index]-128 )
		group.append(dump[ index+1]-256)
		group.append(dump[ index+2]-256)
		group.append(dump[ index+3]-256)
		group.append(dump[ index+4]-256)
		group.extend( [0] * (5 - len(group)) ) 
		group_crc = crc_8(group)
		group_crc = group_crc & 0x7F | 128
		if group_crc== dump[index-1]:		#check if sequence is complete an correct
			del group[0] 			#delete sequence number from return
			return group


try:
	dump=[]
	with open(filename) as f:
		for line in f:
	    		dump.append(int(line)-paket_offest)	

	#find length
	length =findLength(dump)

	#find complete sequences in the dump
	sequence_nr =0
	data=[]
	for i in range(0, length, 4):
		indices=findIndicesofSequencebyNumber( dump, sequence_nr+128)
		data.extend(extractSequence(dump, indices))
		sequence_nr += 1

	#decode data from sequences
	offset=0
	password=""
	pw_length=data[0]
	for i in range(offset,pw_length+1):
		password=password+chr(data[i])
		offset=i
	offset+=1
	token=""
	token_length=data[offset]
	for i in range(offset,offset+token_length+1):
		token=token+chr(data[i])
		offset=i
	offset+=1
	ssid=""
	for i in range(offset,len(data)):
		ssid =ssid+chr(data[i])

	print("Password: " + password)
	print("Token: " + token)
	print("SSID: " + ssid)

except: 
	print("Error while decoding dump")	
	
